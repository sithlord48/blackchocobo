<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>About</name>
    <message>
        <location filename="../src/about.ui" line="17"/>
        <source>About Black Chocobo</source>
        <translation>O Black Chocobo</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="64"/>
        <source>Black Chocobo </source>
        <translation>Black Chocobo</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="143"/>
        <source>&amp;About</source>
        <translation>O Progr&amp;amie</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="149"/>
        <source>The Final Fantasy VII Save Game Editor</source>
        <translation>Edytor Zapisów Gry Final Fantasy VII</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="169"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#508ed8;&quot;&gt;License: GNU GPL Version 3&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#508ed8;&quot;&gt;Licencja: GNU GPL Wersja 3&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="186"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://github.com/sithlord48/blackchocobo&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#508ed8;&quot;&gt;Project Page&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://github.com/sithlord48/blackchocobo&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#508ed8;&quot;&gt;Strona Projektu&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="203"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://sourceforge.net/p/blackchocobo/donate&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#508ed8;&quot;&gt;Support this Project&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://sourceforge.net/p/blackchocobo/donate&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#508ed8;&quot;&gt;Wspomóż Projekt&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="225"/>
        <source>Newest Features</source>
        <translation>Najnowsze Funkcje</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="248"/>
        <source>Simple light and dark color theme option</source>
        <translation>Prosta opcja jasnego i ciemnego motywu kolorystycznego</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="253"/>
        <source>Filename suggestion for Exports</source>
        <translation>Sugestia nazwy pliku dla eksportu</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="258"/>
        <source>Improved Settings Dialog</source>
        <translation>Ulepszone okno dialogowe ustawień</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="263"/>
        <source>Dynamic Language Switching</source>
        <translation>Dynamiczne przełączanie języka</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="268"/>
        <source>Default to Non Native File Dialog</source>
        <translation>Domyślne okno dialogowe pliku innego niż natywny</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="273"/>
        <source>Finalized format Support PS3 export (psv)</source>
        <translation>Sfinalizowany format Obsługa eksportu do PS3 (psv)</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="278"/>
        <source>Finalized format Support PSP exports (vmp) </source>
        <translation>Sfinalizowany format Obsługa eksportu PSP (vmp)</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="283"/>
        <source>New format Support Switch (ff7slot00 - ff7slot09)</source>
        <translation>Przełącznik obsługi nowego formatu (ff7slot00 - ff7slot09)</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="288"/>
        <source>New format Support PGE (ps1, mcs)</source>
        <translation>Obsługa nowego formatu PGE (ps1, mcs)</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="293"/>
        <source>New format Support PDA (psx, mcx, mcb, pda)</source>
        <translation>Obsługa nowego formatu PDA (psx, mcx, mcb, pda)</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="298"/>
        <source>Location Viewer: Added more locations</source>
        <translation>Przeglądarka lokalizacji: dodano więcej lokalizacji</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="307"/>
        <source>C&amp;redits</source>
        <translation>O Nas</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="313"/>
        <source>Giving Credit Where Credit is Due</source>
        <translation>Udzielanie zaszczytów tam, gdzie zaszczyty są należne</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="354"/>
        <source>Programmers</source>
        <translation>Programiści</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="373"/>
        <source>Almost All code and GUI design; </source>
        <translation>Prawie cały kod i projekt GUI;</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="404"/>
        <source>Various code help/debug; Windows QA</source>
        <translation>Różna pomoc/debugowanie kodu; Kontrola jakości systemu Windows</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="435"/>
        <source>Psx Icon Class &amp; PC string converter for Japanese</source>
        <translation>Konwerter Psx Icon Class i PC string dla języka japońskiego</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="471"/>
        <source>Bug Testers</source>
        <translation>Testerzy Błędów</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="493"/>
        <source>Main Bug Tester, for win/gnome + ingame</source>
        <translation>Główny Tester Błędów, dla windows/gnoma + w grze</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="524"/>
        <source>Early Win32 Tester</source>
        <translation>Wczesny Tester Win32</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="555"/>
        <location filename="../src/about.ui" line="586"/>
        <source>Mac Os Tester</source>
        <translation>Mac Os Tester</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="617"/>
        <source>Windows Tester</source>
        <translation>Windows Tester</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="653"/>
        <source>Translators</source>
        <translation>Tłumacze</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="668"/>
        <source>Spanish Translation:</source>
        <translation>Hiszpańskie Tłumaczenie:</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="699"/>
        <source>French Translation:</source>
        <translation>Francuskie Tłumaczenie:</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="730"/>
        <source>Japanese Translation:</source>
        <translation>Japońskie Tłumaczenie:</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="761"/>
        <source>German Translation:</source>
        <translation>Niemieckie Tłumaczenie:</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="807"/>
        <source>Icons Used In this Program</source>
        <translation>Ikony używane w tym programie</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="836"/>
        <source>Buff Icons (and one or two more) by </source>
        <translation>Ikony premii (i jedna lub dwie więcej) przez </translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="893"/>
        <source>&amp;Thanks</source>
        <translation>Podziękowania</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="908"/>
        <source>A Big Thank You  To:</source>
        <translation>Wielkie Podziękowania Dla:</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="939"/>
        <source>Thank you for providing the source to Jenova. This program uses modified versions of Jenova&apos;s savefile struct and Item/Materia lists. Access to its source code saved a lot of time during the early stages of development.</source>
        <translation>Dziękuję za udostępnienie źródła Jenovie. Ten program używa zmodyfikowanych wersji struktury pliku zapisu Jenova i list elementów/materiałów. Dostęp do jego kodu źródłowego zaoszczędził dużo czasu na wczesnych etapach rozwoju.</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="958"/>
        <source>Thank you for all of your help with Black Chocobo. Translating, bug testing, helping with finding data in the savemap, theme testing, etc. You&apos;ve done a lot to help.</source>
        <translation>Dziękuję za wszelką pomoc z Black Chocobo. Tłumaczenie, testowanie błędów, pomoc w znajdowaniu danych w savemapie, testowanie motywów itp. Zrobiłeś wiele, aby pomóc.</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="977"/>
        <source>Thank you for the old checksum program</source>
        <translation>Dziękuję za stary program sum kontrolnych</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="996"/>
        <source>Thank you for helping me see the light of stupid errors.</source>
        <translation>Dziękuję za pomoc w dostrzeżeniu światła głupich błędów.</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="1015"/>
        <source>Thank you for providing a better understanding of Qt&apos;s functions when the help files failed.</source>
        <translation>Dziękujemy za lepsze zrozumienie funkcji Qt, gdy pliki pomocy zawiodły.</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="1034"/>
        <source>Cause she&apos;s just awesome!</source>
        <translation>Bo jest po prostu niesamowita!</translation>
    </message>
    <message>
        <location filename="../src/about.ui" line="1055"/>
        <source>&amp;Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../src/about.cpp" line="31"/>
        <source>Version: %1</source>
        <translation>Wersja: %1</translation>
    </message>
    <message>
        <location filename="../src/about.cpp" line="32"/>
        <source>ff7tk: %1</source>
        <translation>ff7tk: %1</translation>
    </message>
    <message>
        <location filename="../src/about.cpp" line="33"/>
        <source>Qt: %1</source>
        <translation>Qt: %1</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.ui" line="14"/>
        <location filename="../src/mainwindow.cpp" line="865"/>
        <location filename="../src/mainwindow.cpp" line="940"/>
        <location filename="../src/mainwindow.cpp" line="944"/>
        <location filename="../src/mainwindow.cpp" line="3978"/>
        <location filename="../src/mainwindow.cpp" line="4230"/>
        <source>Black Chocobo</source>
        <translation>Black Chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="40"/>
        <source>Party</source>
        <translation>Impreza</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="106"/>
        <source>In Party</source>
        <translation>W Imprezie</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="199"/>
        <source>Click On A Char To edit ===========&gt;</source>
        <translation>Kliknij na znak, aby edytować ===========&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="316"/>
        <source>Selected Character Max Stats/Weapons/Materia</source>
        <translation>Wybrane maksymalne statystyki/bronie/materiały postaci</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="468"/>
        <source>Items</source>
        <translation>Przedmioty</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="492"/>
        <source>Inventory</source>
        <translation>Inwentarz</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="540"/>
        <source>Add Max Of All Items</source>
        <translation>Dodaj maksimum wszystkich przedmiotów</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="553"/>
        <source>Clear All Items</source>
        <translation>Wyczyść wszystkie przedmioty</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="599"/>
        <source>Money</source>
        <translation>Pieniądze</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="631"/>
        <source>Gil</source>
        <translation>Gil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="676"/>
        <source>GP</source>
        <translation>GP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="716"/>
        <location filename="../src/mainwindow.ui" line="745"/>
        <source>Battles</source>
        <translation>Bitwy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="784"/>
        <source>Escapes</source>
        <translation>Ucieczki</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="815"/>
        <source>Turtle Paradise Flyers Collected</source>
        <translation>Zebrano ulotki o Żółwim Raju</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="821"/>
        <source>Search for &quot;Turtle Paradise&quot; using item search mode on the location tab</source>
        <translation>Wyszukaj „Żółwi Raj” za pomocą trybu wyszukiwania przedmiotów na karcie lokalizacji</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="831"/>
        <source>Key Items</source>
        <translation>Kluczowe przedmioty</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="837"/>
        <source>Search For &quot;KeyItem&quot; using item search mode on the location tab</source>
        <translation>Wyszukaj „Kluczowy Przedmiot” za pomocą trybu wyszukiwania przedmiotów na karcie lokalizacji</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="847"/>
        <source>Unused KeyItems</source>
        <translation>Niewykorzystane kluczowe przedmioty</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="856"/>
        <source>Mystery panties</source>
        <translation>Tajemnicze majtki</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="863"/>
        <source>Letter to a Daughter</source>
        <translation>List do Córki</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="870"/>
        <source>Letter to a Wife</source>
        <translation>List do Żony</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="903"/>
        <location filename="../src/mainwindow.ui" line="997"/>
        <location filename="../src/mainwindow.ui" line="4552"/>
        <source>Materia</source>
        <translation>Materia</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="933"/>
        <source>Party&apos;s Materia Stock</source>
        <translation>Zapas materiałów na imprezę</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1002"/>
        <location filename="../src/mainwindow.ui" line="4557"/>
        <source>AP</source>
        <translation>AP</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1037"/>
        <source>Selected Materia Skills and Stat Info</source>
        <translation>Wybrane umiejętności Materia i informacje o statystykach</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1044"/>
        <source>Add Each Materia to Stock (end of list)</source>
        <translation>Dodaj każdy materiał do magazynu (koniec listy)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1051"/>
        <source>Remove ALL Materia </source>
        <translation>Usuń całą Materię</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1062"/>
        <source>Location</source>
        <translation>Lokacja</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1099"/>
        <source>Field Location</source>
        <translation>Lokalizacja pola</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1143"/>
        <source>Worldmap Location</source>
        <translation>Lokalizacja na mapie świata</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1209"/>
        <source>Visible On World Map</source>
        <translation>Widoczne na Mapie Świata</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1265"/>
        <location filename="../src/mainwindow.ui" line="1990"/>
        <source>Buggy</source>
        <translation>Powozik</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1278"/>
        <source>Tiny bronco</source>
        <translation>Małe bronco</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1291"/>
        <location filename="../src/mainwindow.ui" line="1995"/>
        <source>Highwind</source>
        <translation>Silny wiatr</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1304"/>
        <source>Wild chocobo</source>
        <translation>Dzikie chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1341"/>
        <source>Yellow chocobo</source>
        <translation>Żółte chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1357"/>
        <source>Green chocobo</source>
        <translation>Zielone chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1373"/>
        <source>Blue chocobo</source>
        <translation>Niebieskie chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1389"/>
        <source>Black chocobo</source>
        <translation>Black chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1405"/>
        <source>Gold chocobo</source>
        <translation>Złote chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1420"/>
        <source>Party leader</source>
        <translation>Lider Imprezy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1477"/>
        <location filename="../src/mainwindow.ui" line="1662"/>
        <location filename="../src/mainwindow.ui" line="1863"/>
        <location filename="../src/mainwindow.ui" line="2081"/>
        <location filename="../src/mainwindow.ui" line="2282"/>
        <location filename="../src/mainwindow.ui" line="2483"/>
        <location filename="../src/mainwindow.ui" line="6176"/>
        <source>Y: </source>
        <translation>Y: </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1526"/>
        <location filename="../src/mainwindow.ui" line="1717"/>
        <location filename="../src/mainwindow.ui" line="1921"/>
        <location filename="../src/mainwindow.ui" line="2136"/>
        <location filename="../src/mainwindow.ui" line="2337"/>
        <location filename="../src/mainwindow.ui" line="2538"/>
        <source>angle</source>
        <translation>kąt</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1605"/>
        <source>Tiny Bronco / Chocobo</source>
        <translation>Małe bronco / Chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1765"/>
        <location filename="../src/mainwindow.ui" line="2184"/>
        <location filename="../src/mainwindow.ui" line="2385"/>
        <location filename="../src/mainwindow.ui" line="2586"/>
        <source>id</source>
        <translation>id</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1806"/>
        <source>Buggy / Highwind</source>
        <translation>Powozik / Silny wiatr</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1985"/>
        <source>None</source>
        <translation>Żadne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2024"/>
        <location filename="../src/mainwindow.ui" line="2822"/>
        <source>Sub</source>
        <translation>Pod</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2225"/>
        <location filename="../src/mainwindow.ui" line="2827"/>
        <source>Wild Chocobo</source>
        <translation>Dzikie Chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2426"/>
        <source>Diamond / Ultimate / Ruby  Weapon</source>
        <translation>Diamentowa / Ostateczna / Rubinowa Broń</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2793"/>
        <source>sliders show </source>
        <translation>pokaz slajdów</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2807"/>
        <source>Party Leader</source>
        <translation>Lider Imprezy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2812"/>
        <source>TinyBronco/Chocobo</source>
        <translation>Małe bronco / Chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2817"/>
        <source>Buggy/Highwind</source>
        <translation>Powozik / Silny wiatr</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2832"/>
        <source>Diamond / Ultimate / Ruby Weapon</source>
        <translation>Diamentowa / Ostateczna / Rubinowa Broń</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2844"/>
        <source>Right click on map to easily set an item&apos;s
location.</source>
        <translation>Kliknij prawym przyciskiem myszy na mapie, aby łatwo ustawić lokalizację przedmiotu.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2865"/>
        <source>Game Progress</source>
        <translation>Postęp Gry</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2876"/>
        <source>Global Progress</source>
        <translation>Ogólny Postęp</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2894"/>
        <source>Main Progression</source>
        <translation>Główna progresja</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2939"/>
        <source>Disc  #</source>
        <translation>Dysk #</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2971"/>
        <source>Event Progress</source>
        <translation>Postęp wydarzenia</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3016"/>
        <source>Bombing Mission Progress</source>
        <translation>Postęp misji bombardowania</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3034"/>
        <source>Bombing Mission Start Flag</source>
        <translation>Flaga rozpoczęcia misji bombardowania</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3041"/>
        <source>Elevator Door Open</source>
        <translation>Drzwi windy otwarte</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3048"/>
        <source>Elevator On 2nd Floor</source>
        <translation>Winda na drugim piętrze</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3055"/>
        <source>1st Door Open</source>
        <translation>Pierwsze drzwi otwarte</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3062"/>
        <source>2nd Door Opened</source>
        <translation>Drugie drzwi otwarte</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3069"/>
        <source>Post Pan NMKIN_5</source>
        <translation>Post Pan NMKIN_5</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3076"/>
        <source>The Bomb Was Set</source>
        <translation>Bomba została ustawiona</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3083"/>
        <source>Jessie Has Been Unstuck</source>
        <translation>Jessie została odblokowana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3090"/>
        <source>Trigger Game Over (countdown reached 0)</source>
        <translation>Uruchom grę (odliczanie osiągnęło 0)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3100"/>
        <source>Escape From Reactor </source>
        <translation>Ucieczka z reaktora</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3112"/>
        <source>Post Pan MD8_2</source>
        <translation>Post Pan MD8_2</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3119"/>
        <source>Post Electrical Effect MD8_3</source>
        <translation>Efekt poelektryczny MD8_3</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3129"/>
        <source>Midgar Train Flags</source>
        <translation>Flagi pociągu Midgar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3141"/>
        <source>Talked to Biggs</source>
        <translation>Rozmawiałem z Biggsem</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3148"/>
        <source>Talked to Wedge Twice</source>
        <translation>Rozmawiałem z Wedge dwukrotnie</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3155"/>
        <source>Talked to Jessie Before Looking At Map</source>
        <translation>Rozmawiałem z Jessie przed spojrzeniem na mapę</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3162"/>
        <source> Played Video on Train?</source>
        <translation>Odtworzyłeś film w pociągu?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3172"/>
        <source>Sector 7 Trainstation</source>
        <translation>Sektor 7 Dworzec</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3184"/>
        <source>Avalanche had meeting after Bombing Mission</source>
        <translation>Lawina miała spotkanie po zbombardowaniu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3191"/>
        <source>Talked To Trainman 3 times</source>
        <translation>Rozmawiałem z Konduktorem 3 razy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3198"/>
        <source>Pair At Station agree</source>
        <translation>Uzgodnić parę na stacji</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3205"/>
        <source>Set To Reactor 5 Mode</source>
        <translation>Ustaw na tryb Reaktora 5</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3215"/>
        <source>Sector 7 Pillar</source>
        <translation>Sektor 7 Filar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3227"/>
        <source>Avalanche Has Run To Hideout</source>
        <translation>Lawina pobiegła do kryjówki</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3234"/>
        <source>Can Show Pillar Pan Video</source>
        <translation>Może wyświetlać wideo z panoramą słupka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3241"/>
        <source>Barret called us</source>
        <translation>Barret zadzwonił do nas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3248"/>
        <source>Post Pillar Pan Video</source>
        <translation>Post filar Pan wideo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3255"/>
        <source>Talked To soldier two times</source>
        <translation>Rozmawiałem z żołnierzem dwa razy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3265"/>
        <source>Sector 7 - Slums Progress</source>
        <translation>Sektor 7 - Postęp w slumsach</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3283"/>
        <source>Initial Settings</source>
        <translation>Ustawienia początkowe</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3288"/>
        <source>Destroy the Sector 5 Reactor </source>
        <translation>Zniszcz reaktor sektora 5</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3293"/>
        <source>The Plate is Falling</source>
        <translation>Talerz spada</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3304"/>
        <source>Unknowns / Unused</source>
        <translation>Nieznane / Nieużywane</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3318"/>
        <source>ChurchProgress</source>
        <translation>Postęp Kościoła</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3332"/>
        <source>DonProgress</source>
        <translation>Don Postęp</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3556"/>
        <source>Set Replay Mission below to set the game back to that mission. This will automatically set your save location and disc # as well as Quest Progression vars. DO NOT OVERWRITE YOUR CURRENT SAVE when using this feature; I cannot promise that you will be able to play from any replay until the end of the game, or that any given replay will work in your save. This feature is still under development.</source>
        <translation>Ustaw misję powtórki poniżej, aby przywrócić grę do tej misji. Spowoduje to automatyczne ustawienie lokalizacji zapisu i numeru dysku, a także zmienne postępu zadania. NIE NADPISYWAJ BIEŻĄCEGO ZAPISU podczas korzystania z tej funkcji; Nie mogę obiecać, że będziesz mógł grać od dowolnej powtórki do końca gry, ani że dana powtórka będzie działać w twoim zapisie. Ta funkcja jest wciąż w fazie rozwoju.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3566"/>
        <source>Replay Mission</source>
        <translation>Powtórz misję</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3584"/>
        <source>Bombing Mission</source>
        <translation>Misja bombardowania</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3589"/>
        <source>The Church In The Slums</source>
        <translation>Kościół w slumsach</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3594"/>
        <source>Cloud&apos;s Flashback</source>
        <translation>Wspomnienie Cloud</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3599"/>
        <source>The Date Scene</source>
        <translation>Scena randki</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3604"/>
        <source>Aeris Death</source>
        <translation>Śmierć Aeris</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3628"/>
        <source>Apply Selected Replay </source>
        <translation>Zastosuj wybraną powtórkę</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3657"/>
        <source>Other</source>
        <translation>Inne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3683"/>
        <source>Time Played</source>
        <translation>Czas gry</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3709"/>
        <location filename="../src/mainwindow.ui" line="4261"/>
        <source>Hour</source>
        <translation>Godzina</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3745"/>
        <location filename="../src/mainwindow.ui" line="4300"/>
        <source>Min</source>
        <translation>Minuta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3778"/>
        <location filename="../src/mainwindow.ui" line="4345"/>
        <source>Sec</source>
        <translation>Sekunda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="3809"/>
        <source>Love Points</source>
        <translation>Punkty miłości</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4044"/>
        <source>Beat Ruby Weapon</source>
        <translation>Pokonaj Rubinową Broń</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4051"/>
        <source>Beat Emerald Weapon</source>
        <translation>Pokonaj Szmaragdową Broń</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4058"/>
        <source>Have Seen Pandora&apos;s Box</source>
        <translation>Widziałem Puszkę Pandory</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4071"/>
        <source>Can Fight Mystery Ninja in Forests</source>
        <translation>Może walczyć z tajemniczym ninja w lasach</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4078"/>
        <source>Unlocked Vincent</source>
        <translation>Odblokowany Vincent</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4085"/>
        <source>Unlocked Yuffie </source>
        <translation>Odblokowany Yuffie</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4135"/>
        <location filename="../src/mainwindow.ui" line="4154"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resets
When you Pass thru Battle Square&apos;s Door Set Location To &amp;quot;Arena
Lobby&amp;quot; so you can spend
them&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resetowanie
Kiedy przejdziesz przez lokację zestawu drzwi na Placu Bitwy Do &amp;quot;Lobby Areny&amp;quot;,
aby móc je wydać&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4141"/>
        <source>Battle Points</source>
        <translation>Punkty bitewne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4209"/>
        <source>Number of Steps</source>
        <translation>Liczba kroków</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4238"/>
        <source>Countdown Timer</source>
        <translation>Odliczanie czasu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4469"/>
        <source>Cait Sith and Vincent should not be enabled if they are disabled in the Party tab.</source>
        <translation>Cait Sith i Vincent nie powinni być włączani, jeśli są wyłączeni w zakładce Impreza.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4491"/>
        <source>Materia Stolen By Yuffie</source>
        <translation>Materia skradziona przez Yuffie</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4565"/>
        <source>Clear All Stolen Materia</source>
        <translation>Wyczyść całą skradzioną Materię</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4612"/>
        <source>Snowboard Mini Game </source>
        <translation>Minigra snowboardowa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4633"/>
        <source>Beginner Course</source>
        <translation>Kurs dla początkujących</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4659"/>
        <location filename="../src/mainwindow.ui" line="4811"/>
        <location filename="../src/mainwindow.ui" line="4963"/>
        <source>Time:</source>
        <translation>Czas:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4691"/>
        <location filename="../src/mainwindow.ui" line="4843"/>
        <location filename="../src/mainwindow.ui" line="4995"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4723"/>
        <location filename="../src/mainwindow.ui" line="4875"/>
        <location filename="../src/mainwindow.ui" line="5027"/>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4750"/>
        <location filename="../src/mainwindow.ui" line="4902"/>
        <location filename="../src/mainwindow.ui" line="5057"/>
        <source>Score:</source>
        <translation>Wynik:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4773"/>
        <location filename="../src/mainwindow.ui" line="4925"/>
        <location filename="../src/mainwindow.ui" line="5080"/>
        <source>Points</source>
        <translation>Punktów</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4785"/>
        <source>Expert Course</source>
        <translation>Kurs ekspercki</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="4937"/>
        <source>Crazy Course</source>
        <translation>Szalony kurs</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5104"/>
        <source>Coaster Shooter High Scores</source>
        <translation>Najlepsze wyniki strzelanki górskiej</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5167"/>
        <source>1st</source>
        <translation>Pierwszy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5244"/>
        <source>2nd</source>
        <translation>Drugi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5321"/>
        <source>3rd</source>
        <translation>Trzeci</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5356"/>
        <source>Fort Condor</source>
        <translation>Fort Kondor</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5382"/>
        <source>Funds </source>
        <translation>Fundusze</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5418"/>
        <source>Record </source>
        <translation>Rekord</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5465"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5541"/>
        <source>G-Bike High Score</source>
        <translation>Najwyższy wynik G-Bike</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5576"/>
        <source>Have Won the Submarine Game</source>
        <translation>Wygrałem grę w łodzi podwodnej</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5589"/>
        <source>Hex Editor</source>
        <translation>Edytor szesnastkowy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5610"/>
        <source>Playstation Save Info</source>
        <translation>Informacje o zapisie na Playstation</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5634"/>
        <source>PsxName:</source>
        <translation>PsxNazwa:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5685"/>
        <source>No Description Text</source>
        <translation>Brak opisu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5706"/>
        <source>Show:</source>
        <translation>Pokaż:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5735"/>
        <source>PSX Save Data</source>
        <translation>Zapisz dane PSX</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5809"/>
        <source>Test Data</source>
        <translation>Dane testowe</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5846"/>
        <source>Misc</source>
        <translation>Różne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5854"/>
        <source>Battle Love Points</source>
        <translation>Bitwa Punkty Miłości</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="5966"/>
        <source>Ultimate Weapons Hp</source>
        <translation>Ostateczne bronie PŻ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6006"/>
        <source>Played piano durring flashback</source>
        <translation>Gra na pianinie podczas retrospekcji</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6013"/>
        <source>When Box is Partally Checked (&quot;-&quot;) it will
trigger showing that tutorial</source>
        <translation>Gdy pole jest częściowo zaznaczone („-”), będzie wyzwalacz pokazujący ten samouczek</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6034"/>
        <source>Tutorials Seen</source>
        <translation>Widziane samouczki</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6060"/>
        <source>Controling the Sub</source>
        <translation>Kontrolowanie łodzi podwodnej</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6093"/>
        <source>Saving on the World Map</source>
        <translation>Zapisywanie na mapie świata</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6132"/>
        <source>Save Point Location In North Crater</source>
        <translation>Zapisz położenie punktu w kraterze północnym</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6150"/>
        <source>Map Id: </source>
        <translation>Identyfikator mapy: </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6163"/>
        <source>X: </source>
        <translation>X: </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6189"/>
        <source>Z:</source>
        <translation>Z: </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6223"/>
        <source>Region String</source>
        <translation>Region Ciąg</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6287"/>
        <source>S01</source>
        <translation>S01</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6292"/>
        <source>S02</source>
        <translation>S02</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6297"/>
        <source>S03</source>
        <translation>S03</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6302"/>
        <source>S04</source>
        <translation>S04</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6307"/>
        <source>S05</source>
        <translation>S05</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6312"/>
        <source>S06</source>
        <translation>S06</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6317"/>
        <source>S07</source>
        <translation>S07</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6322"/>
        <source>S08</source>
        <translation>S08</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6327"/>
        <source>S09</source>
        <translation>S09</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6332"/>
        <source>S10</source>
        <translation>S10</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6337"/>
        <source>S11</source>
        <translation>S11</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6342"/>
        <source>S12</source>
        <translation>S12</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6347"/>
        <source>S13</source>
        <translation>S13</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6352"/>
        <source>S14</source>
        <translation>S14</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6357"/>
        <source>S15</source>
        <translation>S15</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6398"/>
        <source>Unknown Vars</source>
        <translation>Nieznane zmienne</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6442"/>
        <source>Unknown Var:</source>
        <translation>Nieznana zmienna:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6450"/>
        <location filename="../src/mainwindow.ui" line="6856"/>
        <source>-None-</source>
        <translation>-Brak-</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6720"/>
        <location filename="../src/mainwindow.ui" line="6978"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6725"/>
        <location filename="../src/mainwindow.ui" line="6983"/>
        <source>Hex</source>
        <translation>Szesnatkowy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6730"/>
        <location filename="../src/mainwindow.ui" line="6988"/>
        <source>Dec</source>
        <translation>Dziesiętny</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6735"/>
        <location filename="../src/mainwindow.ui" line="6993"/>
        <source>Bin</source>
        <translation>Binarny</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6759"/>
        <source>&lt;------Left Table------
Select Unknown Var To View
Table Entries are Editable</source>
        <translation>&lt;------Lewa Tabela------
Wybierz nieznaną wartość zmiennej do wyświetlenia
Wpisy w tabeli można edytować</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6774"/>
        <source>------Right Table------&gt;
Select A Slot To Compare
Table is Read Only
Var And Scrolling Synced To Left Table</source>
        <translation>------Prawa Tabela------&gt;
Wybierz slot do porównania
Tabela jest tylko do odczytu
Zmienna i przewijanie zsynchronizowane z lewą tabelą</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="6836"/>
        <source>Compare To Slot</source>
        <translation>Porównaj do slotu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7027"/>
        <source>He&amp;lp</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7034"/>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7051"/>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7064"/>
        <source>&amp;Settings</source>
        <translation>&amp;Ustawienia</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7073"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edycja</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7077"/>
        <source>&amp;Slot Region</source>
        <translation>&amp;Region slotu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7119"/>
        <source>&amp;Quit</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7122"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7134"/>
        <source>&amp;Open</source>
        <translation>&amp;Otwórz</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7137"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7152"/>
        <source>Sa&amp;ve As</source>
        <translation>Zapi&amp;sz jako</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7155"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Shift+S</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7167"/>
        <source>&amp;About</source>
        <translation>O N&amp;as</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7188"/>
        <source>&amp;Select Slot</source>
        <translation>Wybierz slot</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7191"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7203"/>
        <source>&amp;Copy Current Slot</source>
        <translation>&amp;Kopiuj wybrany slot</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7206"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7221"/>
        <source>&amp;Paste Slot</source>
        <translation>Wklej Slot</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7224"/>
        <source>Ctrl+Shift+V</source>
        <translation>Ctrl+Shift+V</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7239"/>
        <source>Clea&amp;r Slot</source>
        <translation>Wyczyść Slot</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7242"/>
        <source>Ctrl+Shift+Del</source>
        <translation>Ctrl+Shift+Del</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7257"/>
        <source>&amp;Previous Slot</source>
        <translation>&amp;Poprzedni Slot</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7260"/>
        <source>Alt+Left</source>
        <translation>Alt+Lewo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7275"/>
        <source>&amp;Next Slot</source>
        <translation>&amp;Następny Slot</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7278"/>
        <source>Alt+Right</source>
        <translation>Alt+Prawo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7290"/>
        <source>&amp;Configure</source>
        <translation>Konfiguracja</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7293"/>
        <source>Application Preferences</source>
        <translation>Preferencje aplikacji</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7296"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7307"/>
        <source>&amp;Us English (NTSC-U)</source>
        <translation>&amp;Us angielski (NTSC-U)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7318"/>
        <source>Uk &amp;English (PAL)</source>
        <translation>Wielka Brytania Angielski (PAL)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7329"/>
        <source>&amp;German (PAL)</source>
        <translation>&amp;Niemiecki (PAL)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7340"/>
        <source>&amp;Spanish (PAL)</source>
        <translation>&amp;Hiszpański (PAL)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7343"/>
        <source>Spanish (PAL)</source>
        <translation>Hiszpański (PAL)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7354"/>
        <source>&amp;Japanese (NTSC-J)</source>
        <translation>&amp;Japoński (NTSC-J)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7357"/>
        <source>Japanese (NTSC-J)</source>
        <translation>Japoński (NTSC-J)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7368"/>
        <source>&amp;International (NTSC-J)</source>
        <translation>&amp;Międzynarodowy (NTSC-J)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7371"/>
        <source>International (NTSC-J)</source>
        <translation>Międzynarodowy (NTSC-J)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7383"/>
        <source>&amp;New Game</source>
        <translation>&amp;Nowa Gra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7386"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7401"/>
        <source>New Game&amp;+ </source>
        <translation>Nowa Gra&amp;+ </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7404"/>
        <source>Ctrl+Shift+=</source>
        <translation>Ctrl+Shift+=</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7416"/>
        <source>E&amp;xport Current Character</source>
        <translation>&amp;Eksportuj bieżący znak</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7419"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7428"/>
        <source>I&amp;mport Current Character</source>
        <translation>&amp;Importuj bieżący znak</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7431"/>
        <source>Ctrl+Ins</source>
        <translation>Ctrl+Ins</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7443"/>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7461"/>
        <source>&amp;Reload</source>
        <translation>&amp;Przeładuj</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7475"/>
        <source>&amp;French (PAL)</source>
        <translation>&amp;Francuski (PAL)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7487"/>
        <source>Create Cloud Save &amp;Folder</source>
        <translation>Utwórz &amp;folder zapisu w chmurze</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7490"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7502"/>
        <source>&amp;Import To Current Slot</source>
        <translation>&amp;Importuj do bieżącego slotu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7505"/>
        <source>Ctrl+Shift+Ins</source>
        <translation>Ctrl+Shift+Ins</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7514"/>
        <source>&amp;Achievement Editor</source>
        <translation>&amp;Edytor osiągnięć</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="7517"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="206"/>
        <location filename="../src/mainwindow.cpp" line="779"/>
        <source>Chocobo</source>
        <translation>Chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="207"/>
        <location filename="../src/mainwindow.cpp" line="780"/>
        <source>Game Options</source>
        <translation>Opcje gry</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="345"/>
        <location filename="../src/mainwindow.cpp" line="346"/>
        <location filename="../src/mainwindow.cpp" line="347"/>
        <location filename="../src/mainwindow.cpp" line="357"/>
        <location filename="../src/mainwindow.cpp" line="358"/>
        <location filename="../src/mainwindow.cpp" line="359"/>
        <source>-Empty-</source>
        <translation>-Pusty-</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="810"/>
        <source>Unsaved Changes</source>
        <translation>Niezapisane zmiany</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="810"/>
        <source>Save Changes to the File:
%1</source>
        <translation>Zapisz zmiany w pliku:
%1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="847"/>
        <location filename="../src/mainwindow.cpp" line="907"/>
        <source>Open Final Fantasy 7 Save</source>
        <translation>Otwórz zapis Final Fantasy 7</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="865"/>
        <location filename="../src/mainwindow.cpp" line="940"/>
        <source>Cannot read file %1:
%2.</source>
        <translation>Nie można odczytać pliku %1:
%2.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="874"/>
        <source>Load Failed</source>
        <translation>Ładowanie nie powiodło się</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="874"/>
        <source>Failed to Load File</source>
        <translation>Nie udało się załadować pliku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="920"/>
        <source>Imported Slot:%2 from %1 -&gt; Slot:%3</source>
        <translation>Importowany slot:%2 z %1 -&gt; Slot:%3</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="922"/>
        <source>Imported %1 -&gt; Slot:%2</source>
        <translation>Importowane %1 -&gt; Slot:%2</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="927"/>
        <source>Error Loading File %1</source>
        <translation>Błąd podczas ładowania pliku %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="935"/>
        <source>Select FF7 Character Stat File</source>
        <translation>Wybierz plik statystyk postaci FF7</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="935"/>
        <location filename="../src/mainwindow.cpp" line="958"/>
        <source>FF7 Character Stat File(*.char)</source>
        <translation>Plik statystyki postaci FF7 (*.char)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="944"/>
        <source>%1:
%2 is Not a FF7 Character Stat File.</source>
        <translation>%1:
%2 nie jest plikiem statystyki znaków FF7.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="957"/>
        <source>Save FF7 Character File</source>
        <translation>Save FF7 Character File</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="961"/>
        <source>Character Export Successful</source>
        <translation>Pomyślny eksport postaci</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="963"/>
        <source>Character Export Failed</source>
        <translation>Eksport postaci nie powiódł się</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="999"/>
        <location filename="../src/mainwindow.cpp" line="1601"/>
        <source>Select A File to Save As</source>
        <translation>Wybierz plik do zapisania jako</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1013"/>
        <source>Export Successful</source>
        <translation>Eksport udany</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1018"/>
        <location filename="../src/mainwindow.cpp" line="1613"/>
        <source>Export Failed</source>
        <translation>Eksport nie udany</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1034"/>
        <source>Save Error</source>
        <translation>Błąd zapisu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1034"/>
        <source>Failed to save file
%1</source>
        <translation>Nie udało się zapisać pliku
% 1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1048"/>
        <source>New Game Created - Using: %1</source>
        <translation>Utworzono nową grę — przy użyciu: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1049"/>
        <location filename="../src/mainwindow.cpp" line="1063"/>
        <source>Builtin Data</source>
        <translation>Wbudowane dane</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1062"/>
        <source>New Game Plus Created - Using: %1</source>
        <translation>Utworzono nową grę Plus — przy użyciu: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1148"/>
        <source>Select Achievement File</source>
        <translation>Wybierz plik osiągnięć</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1148"/>
        <source>Dat File (*.dat)</source>
        <translation>Plik dat (*.dat)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1433"/>
        <source>Current Slot:%1</source>
        <translation>Bieżący slot:%1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1514"/>
        <location filename="../src/mainwindow.cpp" line="1527"/>
        <location filename="../src/mainwindow.cpp" line="1707"/>
        <source>Master</source>
        <translation>Mistrz</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1537"/>
        <location filename="../src/mainwindow.cpp" line="1714"/>
        <source>===Empty Slot===</source>
        <translation>===Pusty slot===</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1611"/>
        <source>Exported Slot:%2 from %1 -&gt; %3</source>
        <translation>Wyeksportowany slot:%2 z %1 -&gt; %3</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1736"/>
        <source>
 Mid-Linked Block </source>
        <translation>
 Blok połączony w środku </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1740"/>
        <source>
 End Of Linked Data</source>
        <translation>
 Koniec połączonych danych</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1745"/>
        <source>(Deleted)</source>
        <translation>(Usunięto)</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/mainwindow.cpp" line="1751"/>
        <source>Game Uses %n Save Block(s)</source>
        <translation>
            <numerusform>Gra używa %n bloków zapisu</numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1757"/>
        <source>
  Next Data Chunk @ Slot:%1</source>
        <translation>
  Następna porcja danych w slocie:%1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2371"/>
        <source>All Materia Added!</source>
        <translation>Dodano Całą Materię!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2385"/>
        <source>Set Save Location: %1</source>
        <translation>Ustaw lokalizację zapisu: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2867"/>
        <source>Replay the bombing mission from right after you get off the train.</source>
        <translation>Powtórz misję bombardowania zaraz po wyjściu z pociągu.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2869"/>
        <source>Meeting Aeris</source>
        <translation>Spotkanie z Aeris</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2871"/>
        <source>This Will Copy Cloud as is to young cloud (caitsith&apos;s slot). Sephiroth&apos;s stats will come directly from the Default Save. Be Sure to back up your CaitSith and Vincent if you want to use them again</source>
        <translation>To skopiuje Cloud tak samo jak młodą chmurę (slot Caitsith). Statystyki Sephirotha będą pochodzić bezpośrednio z domyślnego zapisu. Pamiętaj, aby wykonać kopię zapasową CaitSith i Vincenta, jeśli chcesz ich ponownie użyć</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2873"/>
        <source>Replay the Date Scene, Your Location will be set To The Ropeway Station Talk to man by the Tram to start event. If Your Looking for a special Date be sure to set your love points too.</source>
        <translation>Odtwórz ponownie scenę randkową, Twoja lokalizacja zostanie ustawiona na stacji kolejki linowej Porozmawiaj z człowiekiem przy tramwaju, aby rozpocząć wydarzenie. Jeśli szukasz wyjątkowej randki, ustaw też swoje punkty miłosne.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2875"/>
        <source>Replay the death of Aeris.This option Will remove Aeris from your PHS</source>
        <translation>Odtwórz ponownie śmierć Aeris. Ta opcja usunie Aeris z twojego PHS</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2877"/>
        <source>         INFO ON CURRENTLY SELECTED REPLAY MISSION</source>
        <translation>         INFORMACJE O AKTUALNIE WYBRANEJ MISJI POWTÓRNEJ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2893"/>
        <location filename="../src/mainwindow.cpp" line="2907"/>
        <location filename="../src/mainwindow.cpp" line="2934"/>
        <location filename="../src/mainwindow.cpp" line="2946"/>
        <location filename="../src/mainwindow.cpp" line="2960"/>
        <source>Progression Reset Complete</source>
        <translation>Zakończono resetowanie postępu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3567"/>
        <location filename="../src/mainwindow.cpp" line="3581"/>
        <source>&amp;Place Leader</source>
        <translation>&amp;Umieść lidera</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3568"/>
        <location filename="../src/mainwindow.cpp" line="3584"/>
        <source>Place &amp;Tiny Bronco/Chocobo</source>
        <translation>Umieść &amp;Małe Bronco/Chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3569"/>
        <location filename="../src/mainwindow.cpp" line="3587"/>
        <source>Place &amp;Buggy/Highwind</source>
        <translation>Umieść &amp;Powozik/Silny Wiatr</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3570"/>
        <location filename="../src/mainwindow.cpp" line="3590"/>
        <source>Place &amp;Sub</source>
        <translation>Umieść &amp;Łódź podwodną</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3571"/>
        <location filename="../src/mainwindow.cpp" line="3593"/>
        <source>Place &amp;Wild Chocobo</source>
        <translation>Umieść &amp;Dzikie Chocobo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3572"/>
        <location filename="../src/mainwindow.cpp" line="3596"/>
        <source>Place &amp;Diamond/Ultimate/Ruby Weapon</source>
        <translation>Umieść &amp;Diamentową/Ostateczną/Rubinową broń</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3609"/>
        <source>DON&apos;T USE</source>
        <translation>NIE UŻYWAJ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3621"/>
        <source>All Items Added</source>
        <translation>Wszystkie dodane przedmioty</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3978"/>
        <source>Do You Want To Also Replace %1&apos;s Equipment and Materia?</source>
        <translation>Czy chcesz również wymienić Sprzęt %1 i Materię?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4230"/>
        <source>Unknown Id in Buggy/Highwind Location</source>
        <translation>Nieznany identyfikator w lokalizacji Powozik/Silny Wiatr</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4397"/>
        <source>Turtle Paradise</source>
        <translation>Żółwi Raj</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4404"/>
        <source>KeyItem</source>
        <translation>Kluczowy Przedmiot</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="51"/>
        <source>TRANSLATE TO YOUR LANGUAGE NAME</source>
        <translation>TŁUMACZ NA NAZWĘ JĘZYKA</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../src/options.ui" line="23"/>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="43"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="67"/>
        <source>Application Style</source>
        <translation>Styl Aplikacji</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="86"/>
        <source>System Theme</source>
        <translation>Styl Systemu</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="91"/>
        <source>Dark Theme</source>
        <translation>Ciemny Styl</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="96"/>
        <source>Light Theme</source>
        <translation>Jasny Styl</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="134"/>
        <source>Application Language</source>
        <translation>Język Aplikacji</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="150"/>
        <source>NTSC-U</source>
        <translation>NTSC-U</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="155"/>
        <source>PAL-E</source>
        <translation>PAL-E</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="160"/>
        <source>PAL-FR</source>
        <translation>PAL-FR</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="165"/>
        <source>PAL-DE</source>
        <translation>PAL-DE</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="170"/>
        <source>PAL-ES</source>
        <translation>PAL-ES</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="175"/>
        <source>NTSC-J</source>
        <translation>NTSC-J</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="180"/>
        <source>NTSC-JI</source>
        <translation>NTSC-JI</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="218"/>
        <source>Application Color Scheme</source>
        <translation>Schemat kolorów aplikacji</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="227"/>
        <source>Application Scale</source>
        <translation>Skala aplikacji</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="282"/>
        <source>100%</source>
        <translation>100%</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="327"/>
        <source>Normal Scale:</source>
        <translation>Skala normalna:</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="403"/>
        <source>Current Scale</source>
        <translation>Aktualna skala</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="476"/>
        <source>New Game Default Region </source>
        <translation>Domyślny region nowej gry</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="491"/>
        <source>Edit the Places show in sidebar on file dialogs</source>
        <translation>Edytuj miejsca wyświetlane na pasku bocznym w oknach dialogowych plików</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="494"/>
        <source>Edit Places</source>
        <translation>Edytuj miejsca</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="507"/>
        <source>Use Native File Dialogs</source>
        <translation>Użyj natywnych okien dialogowych plików</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="515"/>
        <source>File Options</source>
        <translation>Opcje Plików</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="526"/>
        <source>Default Path For Loading All Saves</source>
        <translation>Domyślna ścieżka do wczytywania wszystkich zapisów</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="575"/>
        <source>Path For Emulator Memory Cards  </source>
        <translation>Ścieżka do kart pamięci emulatora  </translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="613"/>
        <source>Path For PC (.ff7) Saves</source>
        <translation>Ścieżka Zapisu dla PC (.ff7)</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="647"/>
        <source>Path For Character Stat Files</source>
        <translation>Ścieżka do plików statystyk postaci</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="686"/>
        <source>Override Builtin New Game Data</source>
        <translation>Nadpisz wbudowane nowe dane gry</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="707"/>
        <source>Raw Psx Files Only!</source>
        <translation>Tylko surowe pliki PSX!</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="736"/>
        <source>Editing Options</source>
        <translation>Opcje edycji</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="775"/>
        <source>Show Experimental TestData Tab</source>
        <translation>Pokaż kartę eksperymentalnych danych testowych</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="805"/>
        <source>Editable Combo for Materia and Items</source>
        <translation>Edytowalne kombinacje materiałów i przedmiotów</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="832"/>
        <source>CharEditor - Simulate Leveling Up / Down</source>
        <translation>CharEditor - Symuluj zdobywanie poziomów w górę / w dół</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="859"/>
        <source>CharEditor - Advanced Mode</source>
        <translation>Edytor znaków — tryb zaawansowany</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="892"/>
        <source>Chocobo Manager - Show Pcount / Personality</source>
        <translation>Chocobo Manager - Pokaż Pcount / Osobowość</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="919"/>
        <source>Field Location - Show Map/X/Y/T/D</source>
        <translation>Lokalizacja pola — Pokaż mapę/X/Y/T/D</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="946"/>
        <source>World Map Viewer- Show int SpinBoxes for Leader ID and buggy ID</source>
        <translation>Przeglądarka map świata - Pokaż int SpinBoxa dla identyfikatora lidera i identyfikatora buggy</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="973"/>
        <source>Game Progress - Show Untested</source>
        <translation>Postęp gry — pokaż nieprzetestowane</translation>
    </message>
    <message>
        <location filename="../src/options.ui" line="1000"/>
        <source>Options - Always Show Controller Mapping </source>
        <translation>Opcje - Zawsze pokazuj mapowanie kontrolera </translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="205"/>
        <source>Reset values to defaults</source>
        <translation>Zresetuj wartości do wartości domyślnych</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="206"/>
        <source>Reset values to stored settings</source>
        <translation>Zresetuj wartości do zapisanych ustawień</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="207"/>
        <source>Close and save changes</source>
        <translation>Zamknij i zapisz zmiany</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="208"/>
        <source>Close and forget changes</source>
        <translation>Zamknij i zapomnij o zmianach</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="214"/>
        <source>Select A Directory To Save FF7 PC Saves</source>
        <translation>Wybierz katalog, aby zapisać zapisy PC FF7</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="221"/>
        <source>Select A Directory To Save mcd/mcr saves</source>
        <translation>Wybierz katalog, aby zapisać zapisy mcd/mcr</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="228"/>
        <source>Select A Directory To Load FF7 PC Saves From</source>
        <translation>Wybierz katalog, z którego chcesz załadować Zapisy FF7 PC</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="235"/>
        <source>Select A Default Save Game (Must Be Raw PSX)</source>
        <translation>Wybierz domyślny zapis gry (musi to być surowy PSX)</translation>
    </message>
    <message>
        <location filename="../src/options.cpp" line="242"/>
        <source>Select A Location To Save Character Stat Files</source>
        <translation>Wybierz lokalizację, aby zapisać pliki statystyk postaci</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/bcdialog.cpp" line="149"/>
        <source>Edit Places</source>
        <translation>Edytuj miejsca</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="160"/>
        <source>Add a place</source>
        <translation>Dodaj miejsce</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="162"/>
        <source>Select a path to add to places</source>
        <translation>Wybierz ścieżkę do dodania do miejsc</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="167"/>
        <source>Remove selected place</source>
        <translation>Usuń wybrane miejsce</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="177"/>
        <source>Restore default places</source>
        <translation>Przywróć domyślne miejsca</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="178"/>
        <source>Reset values to stored settings</source>
        <translation>Zresetuj wartości do zapisanych ustawień</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="179"/>
        <source>Save Changes</source>
        <translation>Zapisz zmiany</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="180"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="226"/>
        <source>Adjust the play time?</source>
        <translation>Dostosować czas odtwarzania?</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="227"/>
        <source>Old region was %1hz
New region is %2hz</source>
        <translation>Stary region to %1hz
Nowy region to %2hz</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="228"/>
        <source>PAL -&gt; NTSC Conversion</source>
        <translation>Konwersja PAL -&gt; NTSC</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="228"/>
        <source>NTSC -&gt; PAL Conversion</source>
        <translation>Konwersja NTSC -&gt; PAL</translation>
    </message>
    <message>
        <location filename="../src/bcdialog.cpp" line="247"/>
        <source>Places</source>
        <translation>Miejsca</translation>
    </message>
</context>
<context>
    <name>UndoStack</name>
    <message>
        <location filename="../qhexedit/commands.cpp" line="114"/>
        <source>Inserting %1 bytes</source>
        <translation>Wstawianie %1 bajtów</translation>
    </message>
    <message>
        <location filename="../qhexedit/commands.cpp" line="136"/>
        <source>Delete %1 chars</source>
        <translation>Usuń %1 znaków</translation>
    </message>
    <message>
        <location filename="../qhexedit/commands.cpp" line="161"/>
        <source>Overwrite %1 chars</source>
        <translation>Nadpisz %1 znaków</translation>
    </message>
</context>
<context>
    <name>achievementDialog</name>
    <message>
        <location filename="../src/achievementdialog.cpp" line="32"/>
        <source>Achievement Editor</source>
        <translation>Edytor osiągnięć</translation>
    </message>
    <message>
        <location filename="../src/achievementdialog.cpp" line="43"/>
        <source>Close and save changes</source>
        <translation>Zamknij i zapisz zmiany</translation>
    </message>
    <message>
        <location filename="../src/achievementdialog.cpp" line="44"/>
        <source>Close and forget changes</source>
        <translation>Zamknij i nie zapisuj</translation>
    </message>
    <message>
        <location filename="../src/achievementdialog.cpp" line="49"/>
        <source>Failed To Save File</source>
        <translation>Nie udało się zapisać pliku</translation>
    </message>
    <message>
        <location filename="../src/achievementdialog.cpp" line="49"/>
        <source>Failed To Write File
File:%1</source>
        <translation>Nie udało się zapisać pliku
Plik: %1</translation>
    </message>
</context>
<context>
    <name>errbox</name>
    <message>
        <location filename="../src/errbox.cpp" line="29"/>
        <source>View Anyway</source>
        <translation>Pokaż Zawsze</translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="30"/>
        <source>E&amp;xport Slot</source>
        <translation>E&amp;xport Slot</translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="40"/>
        <source>Non Final Fantasy VII Slot</source>
        <translation>Slot Nie z Final Fantasy VII</translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="86"/>
        <source>Slot:%1
</source>
        <translation>Slot: %1
</translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="94"/>
        <source>       Mid-Linked Block</source>
        <translation>       Środkowy-Połączony Blok</translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="95"/>
        <source>    Mid-Linked Block (Deleted)</source>
        <translation>       Środkowy-Połączony Blok (Usunięty)</translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="96"/>
        <source>      End Of Linked Blocks</source>
        <translation>       Koniec Połączonych Bloków</translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="97"/>
        <source>      End Of Linked Blocks (Deleted)</source>
        <translation>       Koniec Połączonych Bloków (Usunięte)</translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="98"/>
        <source>ERROR</source>
        <translation>BŁĄD</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/errbox.cpp" line="107"/>
        <source>
 Game Uses %n Save Block(s)</source>
        <translation>
            <numerusform> Gra Używa %n Zapisanych Bloków</numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/errbox.cpp" line="111"/>
        <source>
   Next Data Chunk @ Slot:%1</source>
        <translation>   Następna Porcja Danych @ Slot:%1</translation>
    </message>
</context>
</TS>
